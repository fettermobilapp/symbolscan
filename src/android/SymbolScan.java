package com.scanapp;


import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.LOG;

import android.content.Context;


import android.os.AsyncTask;
import android.view.KeyEvent;
import android.view.View;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.view.View.OnKeyListener;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

import com.symbol.emdk.EMDKManager;
import com.symbol.emdk.EMDKManager.EMDKListener;
import com.symbol.emdk.EMDKManager.FEATURE_TYPE;
import com.symbol.emdk.EMDKResults;
import com.symbol.emdk.barcode.BarcodeManager;
import com.symbol.emdk.barcode.ScanDataCollection;
import com.symbol.emdk.barcode.ScanDataCollection.LabelType;
import com.symbol.emdk.barcode.ScanDataCollection.ScanData;
import com.symbol.emdk.barcode.Scanner;
import com.symbol.emdk.barcode.Scanner.DataListener;
import com.symbol.emdk.barcode.Scanner.StatusListener;
import com.symbol.emdk.barcode.Scanner.TriggerType;
import com.symbol.emdk.barcode.ScannerConfig;
import com.symbol.emdk.barcode.ScannerException;
import com.symbol.emdk.barcode.ScannerInfo;
import com.symbol.emdk.barcode.ScannerResults;
import com.symbol.emdk.barcode.StatusData;
import com.symbol.emdk.barcode.StatusData.ScannerStates;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;



public class SymbolScan extends CordovaPlugin implements EMDKListener,
        StatusListener, DataListener, OnCheckedChangeListener, OnKeyListener{

    private CallbackContext scanCallbackContext;


    // Declare a variable to store EMDKManager object
    private EMDKManager emdkManager = null;

    // Declare a variable to store Barcode Manager object
    private BarcodeManager barcodeManager = null;

    // Declare a variable to hold scanner device to scan
    private Scanner scanner = null;

    // Text view to display status of EMDK and Barcode Scanning Operations


    private Context appContext;
    // Boolean to explain whether the scanning is in progress or not at any
    // specific point of time
    boolean isScanning = false;

    // Array Adapter to hold arrays that are used in various drop downs
    private ArrayAdapter<String> spinnerDataAdapter;

    // List of supported scanner devices
    private List<ScannerInfo> deviceList;

    // Provides current scanner index in the device Selection Spinner
    private int scannerIndex = 0;


public SymbolScan(){
    scanCallbackContext=null;
}



    public void pluginInitialize() {
        LOG.i(this.getClass().getName(), "Initialize PowerManager");

        super.pluginInitialize();
        this.appContext = this.cordova.getActivity().getApplicationContext();

        // Reference to UI elements




        // The EMDKManager object will be created and returned in the callback.
        EMDKResults results = EMDKManager.getEMDKManager(
                appContext, this);
        // Check the return status of getEMDKManager and update the status Text
        // View accordingly
        if (results.statusCode != EMDKResults.STATUS_CODE.SUCCESS) {
           LOG.e("SymbolScan","EMDKManager Request Failed");
        }
    }



    // Disable the scanner instance
    private void deInitScanner() {

        if (scanner != null) {
            try {
                scanner.cancelRead();

                scanner.removeDataListener(this);
                scanner.removeStatusListener(this);
                scanner.disable();

            } catch (ScannerException e) {
                // TODO Auto-generated catch block
                LOG.e("SymbolScan","Status: " + e.getMessage());
            }
            scanner = null;
        }
    }

    // Method to initialize and enable Scanner and its listeners
    private void initializeScanner() throws ScannerException {

        if (deviceList.size() != 0) {
            scanner = barcodeManager.getDevice(deviceList.get(scannerIndex));
        } else {
            LOG.e("SymbolScan","Status: "
                            + "Failed to get the specified scanner device! Please close and restart the application.");
        }

        if (scanner != null) {

            // Add data and status listeners
            scanner.addDataListener(this);
            scanner.addStatusListener(this);

            // The trigger type is set to HARD by default and HARD is not
            // implemented in this release.
            // So set to SOFT_ALWAYS
            scanner.triggerType = TriggerType.SOFT_ALWAYS;

            try {
                // Enable the scanner
                scanner.enable();
            } catch (ScannerException e) {
                // TODO Auto-generated catch block
                LOG.e("SymbolScan","Status: " + e.getMessage());
            }

        }

    }

    // Sets the user selected Profile
    public void setProfile() {
        try {

            ScannerConfig config = scanner.getConfig();

            // Set code11


            // Set the Scan Tone selected from the Scan Tone Spinner
                // Silent Mode (No scan tone will be played)
                config.scanParams.decodeAudioFeedbackUri = "";

            scanner.setConfig(config);


        } catch (Exception e) {

            LOG.e("SymbolScan",e.toString());
        }
    }


    public void onClosed() {
        // TODO Auto-generated method stub
        // The EMDK closed abruptly. // Clean up the objects created by EMDK
        // manager
        if (this.emdkManager != null) {

            this.emdkManager.release();
            this.emdkManager = null;
        }

    }

    @Override
    public void onOpened(EMDKManager emdkManager) {
        // TODO Auto-generated method stub
        this.emdkManager = emdkManager;

        // Get the Barcode Manager object
        barcodeManager = (BarcodeManager) this.emdkManager
                .getInstance(FEATURE_TYPE.BARCODE);

        try {
            // Get the supported scanner devices
            enumerateScannerDevices();
            initializeScanner();
            setProfile();
        } catch (ScannerException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    // Go through and get the available scanner devices
    private void enumerateScannerDevices() {

        if (barcodeManager != null) {

            List<String> friendlyNameList = new ArrayList<String>();
            int spinnerIndex = 0;
            // Set the default selection in the spinner
            int defaultIndex = 0;

            deviceList = barcodeManager.getSupportedDevicesInfo();

            if (deviceList.size() != 0) {

                Iterator<ScannerInfo> it = deviceList.iterator();
                while (it.hasNext()) {
                    ScannerInfo scnInfo = it.next();
                    friendlyNameList.add(scnInfo.getFriendlyName());
                    if (scnInfo.isDefaultScanner()) {
                        defaultIndex = spinnerIndex;
                    }
                    ++spinnerIndex;
                }
            } else {
                LOG.e("SymbolScan","Status: "
                                + "Failed to get the list of supported scanner devices! Please close and restart the application.");
            }



        }
    }

    // This is a callback method when user presses any hardware button on the
    // device






    @Override
    public void onData(ScanDataCollection scanDataCollection) {
        // TODO Auto-generated method stub
        // Use the scanned data, process it on background thread using AsyncTask
        // and update the UI thread with the scanned results
        new AsyncDataUpdate().execute(scanDataCollection);
    }

    // Update the scan data on UI
    int dataLength = 0;

    // AsyncTask that configures the scanned data on background
    // thread and updated the result on UI thread with scanned data and type of
    // label
    private class AsyncDataUpdate extends
            AsyncTask<ScanDataCollection, Void, String> {

        @Override
        protected String doInBackground(ScanDataCollection... params) {
            ScanDataCollection scanDataCollection = params[0];

            // Status string that contains both barcode data and type of barcode
            // that is being scanned
            String statusStr = "";

            // The ScanDataCollection object gives scanning result and the
            // collection of ScanData. So check the data and its status
            if (scanDataCollection != null
                    && scanDataCollection.getResult() == ScannerResults.SUCCESS) {

                ArrayList<ScanData> scanData = scanDataCollection.getScanData();

                // Iterate through scanned data and prepare the statusStr
                for (ScanData data : scanData) {
                    // Get the scanned data
                    String barcodeDate = data.getData();
                    // Get the type of label being scanned
                    LabelType labelType = data.getLabelType();
                    // Concatenate barcode data and label type
                    statusStr = barcodeDate + " " + labelType;
                }
            }

            // Return result to populate on UI thread
            return statusStr;
        }

        @Override
        protected void onPostExecute(String result) {
            JSONObject info= new JSONObject();
            try{
                info.put("signal", new String(result));
                sendSignal(info, true);
            }
            catch(JSONException ex){
                LOG.e("SymbolScan",ex.getMessage());
            }
          //  dataView.append(result + "\n");
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    @Override
    public void onStatus(StatusData statusData) {
        // TODO Auto-generated method stub
        // process the scan status event on the background thread using
        // AsyncTask and update the UI thread with current scanner state
        new AsyncStatusUpdate().execute(statusData);

    }

    // AsyncTask that configures the current state of scanner on background
    // thread and updates the result on UI thread
    private class AsyncStatusUpdate extends AsyncTask<StatusData, Void, String> {

        @Override
        protected String doInBackground(StatusData... params) {
            // Get the current state of scanner in background
            StatusData statusData = params[0];
            String statusStr = "";
            ScannerStates state = statusData.getState();
            // Different states of Scanner
            switch (state) {
                // Scanner is IDLE
                case IDLE:
                    statusStr = "The scanner enabled and its idle";
                    isScanning = false;
                    break;
                // Scanner is SCANNING
                case SCANNING:
                    statusStr = "Scanning..";
                    isScanning = true;
                    break;
                // Scanner is waiting for trigger press
                case WAITING:
                    statusStr = "Waiting for trigger press..";
                    break;
                default:
                    break;
            }
            // Return result to populate on UI thread
            return statusStr;
        }

        @Override
        protected void onPostExecute(String result) {
            // Update the status text view on UI thread with current scanner
            // state

            JSONObject info= new JSONObject();
            try{
                info.put("signal", new String(result));
                sendSignal(info, true);
            }
            catch(JSONException ex){
                LOG.e("SymbolScan",ex.getMessage());
            }

        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }


    public void onStop() {

        deInitScanner();
    }


    protected void onDestroy() {
        super.onDestroy();

        if (barcodeManager != null)
            barcodeManager = null;

        if (emdkManager != null) {

            // Clean up the objects created by EMDK manager
            emdkManager.release();
            emdkManager = null;
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        setProfile();
    }





    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        // check for scanner hard key press.
        if ((keyCode == KeyEvent.KEYCODE_BUTTON_L1)
                || (keyCode == KeyEvent.KEYCODE_BUTTON_R1)) {

            // Skip the key press if the repeat count is not zero.
            if (event.getRepeatCount() != 0) {
                return true;
            }

            try {
                if (scanner == null) {
                    initializeScanner();
                }

                if ((scanner != null) && (isScanning == false)) {
                    // Starts an asynchronous Scan. The method will not turn on
                    // the scanner. It will, however, put the scanner in a state
                    // in which the scanner can be turned ON either by pressing
                    // a hardware trigger or can be turned ON automatically.
                    scanner.read();
                }

            } catch (Exception e) {
                // Display if there is any exception while performing operation
                LOG.e("SymbolScan",e.getMessage());
            }
return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    // This is a callback method when user releases any hardware button on the
    // device
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {

        // check for scanner trigger key press.
        if ((keyCode == KeyEvent.KEYCODE_BUTTON_L1)
                || (keyCode == KeyEvent.KEYCODE_BUTTON_R1)) {

            // Skip the key press if the repeat count is not zero.
            if (event.getRepeatCount() != 0) {
                return true;
            }

            try {
                if ((scanner != null) && (isScanning == true)) {
                    // This Cancels any pending asynchronous read() calls
                    scanner.cancelRead();
                }
            } catch (Exception e) {
                LOG.e("SymbolScan",e.getMessage());
            }
return true;
        }
        return super.onKeyUp(keyCode, event);
    }






    private void sendSignal(JSONObject info, boolean keepCallback)
    {
        if( this.scanCallbackContext != null ){
            PluginResult result= new PluginResult(PluginResult.Status.OK, info);
            result.setKeepCallback(keepCallback);
            this.scanCallbackContext.sendPluginResult(result);
        }
    }


    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {

        // Check the action
        if( action.equals("start") ){

            // Check if the plugin is listening the volume button events
            if( this.scanCallbackContext != null ){

                callbackContext.error("Volume buttons listener already running");
                return true;
            }

            // Get the reference to the callbacks and start the listening process
            this.scanCallbackContext= callbackContext;
            this.webView.setOnKeyListener(this);

            // Don't return any result now
            PluginResult pluginResult= new PluginResult(PluginResult.Status.NO_RESULT);
            pluginResult.setKeepCallback(true);
            this.scanCallbackContext.sendPluginResult(pluginResult);
            return true;
        }
        else if( action.equals("stop") ){

            // Erase the callbacks reference and stop the listening process
            sendSignal(new JSONObject(), false); // release status callback in Javascript side
            this.scanCallbackContext= null;
            this.webView.setOnKeyListener(null);
            callbackContext.success();
            return true;
        }

        return false;
    }

}
